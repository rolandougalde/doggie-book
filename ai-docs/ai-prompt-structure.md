# AI Prompt structure

AI prompts shoul be well constructed using the propper instructions, in order to get desired results.
From the required aspect (task) the most important, context and example data, to complementary information.

**Basic form:**
- Task (required)
- Context (important)
- Examples (important)

**Complementary data:**
- Person (Bonus)
- Format (Bonus)
- Tone (Bonus)

## Task

Use a *verb* like: write, redact, analyze, read, design, program, plan, etc..

**Single task:** ```write and e-mail.```
**Multi task:** ```analyze a given text and write a recuperation e-mail.```

## Context
    - Who we are?
    - What we want?
    - Current situation.

## Examples
    - Write a CV using Star leadership method.
    - Analize a text and write a Facebook post using fear method.

## Person

    - Who the AI should act as, for example an ITOps or DevOps professional.
    - Can be a Linkedin recruiter.
    - AI can also assume a fictional personality.

## Format 

Format results, for example: ```write a list with colums: name, description and prodcut number```

## Tone

Specify a tone, formal, casual, friendly, energic, aggresive, authoritative, unsecure, etc..