# Debian Linux 12.6.0 Maintnance

> This lab was executed using **root** user. 

```# sudo su -```

## Check the startup time

```shell
$ systemd-analyze
```

## Check resoures

```shell
$ free -m
$ df -h
$ htop
```

## Check the system logs

```shell
$ tail -f /var/log/syslog
```

## Truncate system logs
```shell
$ journalctl --vacuum-time=1day
```

## Check the running process

```shell
$ ps axf
```

## Clean and update packages

```shell

$ apt-get clean && sudo apt-get update

```

## Remove unneeded packages

```shell
$ apt-get remove --purge intel-microcode
$ apt-get remove --purge linux-firmware
$ apt remove --purge linux-image-amd64 && 
$ apt install linux-image-cloud-amd64  &&
$ apt install swapspace
$ apt install sysstat
```

## Update distro

```shell
apt-get dist-upgrade
```

## Change grub startup delay

```shell
$ nano /etc/default/grub
```
> ```GRUB_TIMEOUT=0```

## List installed packages

```shell
$ dpkg --get-selections | grep php*
```

# List directory by size sumary

```shell
$ du -sBM * | sort -nr
```
