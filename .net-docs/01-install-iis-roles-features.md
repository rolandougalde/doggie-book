# Blazor ASP .Net Core 

Deploy an Blazor ASP .Net Core app on Windows 2012-R2, with IIS 8.5.x

## IIS Roles & Features

Install the needed despendencies on the *"Features"*, *"Add Roles and Features Wizard"*:

### Server Roles

- [X] Web Server (IIS)
    - [X] Web Server
        - [X] Common HTTP Features
        - [X] Static Content
            - [X] Default Document
            - [X] Directory Browsing
            - [X] HTTP Errors
            - [X] Static Content
            - [X] HTTP Redirection
            - [ ] WebDAV Publishing
        - [X] Health and Diagnostics
            - [X] HTTP Logging
            - [ ] Custom Logging
            - [X] Logging Tools
            - [ ] ODBC Logging
            - [X] Request Monitor
            - [ ] Tracing
        - [X] Performance
            - [X] Static Content Compression
            - [X] Dynamic Content Compression
        - [X] Security
            - [X] Request Filtering
            - [X] Basic Authentication
            - [ ] Centralized SSL Certificate Support
            - [ ] Client Certificate Mapping Authentication
            - [ ] Digest Authentication
            - [ ] IIS Client Certificate Mapping Authentication
            - [ ] IP and Domain Restrictions
            - [ ] URL Authorization
            - [X] Windows Authentication
        - [X] Application Development :point_left:
            - [X] .NET Extensibility 3.5 :point_left:
            - [X] .NET Extensibility 4.5 :point_left:
            - [X] Application Initialization :point_left:
            - [X] ASP :point_left:
            - [X] ASP.NET 3.5 :point_left:
            - [X] ASP.NET 4.5 :point_left:
            - [X] CGI
            - [X] ISAPI Extensions
            - [X] ISAPI Filters
            - [X] Server Side Includes
            - [X] WebSocket Protocol :point_left:
        - [ ] FTP Server
            - [ ] FTP Service
            - [ ] FTP Extensibility
    - [X] Management Tools
        - [X] IIS Management Console
        - [ ] IIS 6 Management Compatibility
            - [ ] IIS 6 Metabase Compatibility
            - [ ] IIS 6 Management Console
            - [ ] IIS 6 Scripting Tools
            - [ ] IIS WMI Compatibility
        - [ ] IIS Management Scripts and Tools
        - [ ] Management Service

### Server Features

- [X] .NET Framework 3.5 :point_left:
    - [X] .NET Framework 3.5 (includes .NET 2.0 and 3,0) :point_left:
    - [ ] HTTP Activation
    - [X] Non-HTTP Activation
- [x] .NET Framework 4.5 Features :point_left:
    - [X] .NET Framework 4.5 :point_left:
    - [X] ASP.NET 4.5 :point_left:
    - [X] WCF Services :point_left:
        - [X] HTTP Activation
        - [X] Message Queuing (MSMQ) Activation
        - [X] Named Pipe Activation
        - [X] TCP Activation
        - [X] TCP Port Sharing :point_left:
