# ASP .NET Core 8

## Install Microsoft:registered: Frameworks

[Download .NET 8.0](https://dotnet.microsoft.com/en-us/download/dotnet/8.0)

Building lab enviroment I installed three packages:

```bash
aspnetcore-runtime-8.x.x-win-x64.exe
dotnet-hosting-8.x.x-win.exe
dotnet-sdk-8.x.x-win-x64.exe
```

At this time is a good idea to reboot your server.

## Deploy the ASP app

On IIS create a new site, browse your ASP code, then look for the *Application Pool* and choose, "*No Managed Code*" option.

Once you're ready, restart IIS:

```bash
net stop was /y
net start w3svc
```

Look for more resouces at:

[Microsof Learn | Blazor Documentation](https://learn.microsoft.com/en-us/aspnet/core/blazor/?view=aspnetcore-8.0)

[Microsof Learn | Build web apps with Blazor](https://learn.microsoft.com/en-us/training/paths/build-web-apps-with-blazor/)